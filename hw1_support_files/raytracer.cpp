#include <iostream>
#include <float.h>
#include "parser.h"
#include "ppm.h"
#include "raytracer.h"


typedef unsigned char RGB[3];
unsigned long cameraCount;
unsigned long sphereCount;
unsigned long triangleCount;
unsigned long pointLightsCount;

//bool debugFlag = 0;
class Painter {
public:
    int nx;
    int ny;

    parser::Vec3f e;
    parser::Vec3f u;
    parser::Vec3f v;
    parser::Vec3f w;
    parser::Camera *camera;
    float distance;
    parser::Vec4f *imagePlane;
    parser::Vec3f m;
    parser::Vec3f q;


    Painter(parser::Camera *camera) {
        this->nx = camera->image_width;
        this->ny = camera->image_height;
        this->camera = camera;
        this->e = camera->position;
        this->v = camera->up;
        this->w = -camera->gaze;
        this->u = uCalculator();
        this->distance = camera->near_distance;
        this->imagePlane = &(camera->near_plane);
        this->m = this->e + camera->gaze * this->distance;
        this->q = this->m + (this->imagePlane->x * this->u) + (this->imagePlane->w * this->v);

    };

    parser::Vec3f uCalculator() {
        parser::Vec3f ret = this->camera->gaze % this->v;
        return (ret * (1 / getVecLength(ret)));
    }

    parser::Vec3f sCalculator(unsigned int i, unsigned int j) {
        return this->q + ((this->imagePlane->y - this->imagePlane->x) * (0.5 + i) * (1.0 / this->nx)) * this->u
               - ((this->imagePlane->w - this->imagePlane->z) * (0.5 + j) * (1.0 / this->ny)) * this->v;
    }

    void execute() {
        auto *image = new unsigned char[this->nx * this->ny * 3];
        unsigned int imageIndex = 0;
        auto *ray = new Ray;
        auto hitObj = new Obj(nullptr, EMPTY, DBL_MAX);
        parser::Vec3f s;
        parser::Sphere *sphere = nullptr;
        parser::Triangle *triangle = nullptr;
        parser::Vec3i calculatedColor;
        double t;

        for (int j = 0; j < ny; ++j) {
            for (int i = 0; i < nx; ++i) {
//                if(j == 477 && i==12){
//                    debugFlag = 1;
//                }else{
//                    debugFlag = 0;
//                }

                s = this->sCalculator(i, j);
                ray->d = s - this->e;
                ray->e = this->e;
                *hitObj = {nullptr, EMPTY, DBL_MAX};

                this->objCatcher(ray, hitObj, 1);

                if (hitObj->objType == EMPTY) {
                    calculatedColor.x = (unsigned char) scene.background_color.x;
                    calculatedColor.y = (unsigned char) scene.background_color.y;
                    calculatedColor.z = (unsigned char) scene.background_color.z;
                } else {
                    calculatedColor = normalizeRGB(getShading(hitObj, ray, 0));
                }
                image[imageIndex++] = (unsigned char) calculatedColor.x;
                image[imageIndex++] = (unsigned char) calculatedColor.y;
                image[imageIndex++] = (unsigned char) calculatedColor.z;
            }
        }

        write_ppm(this->camera->image_name.c_str(), image, this->nx, this->ny);
    }

    double sphereIntersect(Ray *ray, parser::Sphere *sphere, double t_0, double t_max) {
        parser::Vec3f sphere_center = scene.vertex_data[(sphere->center_vertex_id - 1)];

        double discriminant = pow((ray->d * (ray->e - sphere_center)), 2)
                              - pow2Dot(ray->d) * (pow2Dot(ray->e - sphere_center) - pow(sphere->radius, 2));

        if (discriminant < 0) {
            return 0;
        }

        double t1 = (-(ray->d * (ray->e - sphere_center)) + sqrt(discriminant)) / (pow2Dot(ray->d));
        double t2 = t1 - 2 * ((sqrt(discriminant)) / (pow2Dot(ray->d)));

        if ((t1 < t_0 || t1 > t_max) && (t2 < t_0 || t2 > t_max)) {
            return 0;
        }

        if (t1 > 0 && t1 < t2) {
            return t1;
        }

        if (t2 > 0 && t2 < t1) {
            return t2;
        }

        return 0;
    }

    double triangleIntersect(Ray *ray, parser::Triangle *triangle, double t_0, double t_max, unsigned char mode) {
        parser::Vec3f t_a = scene.vertex_data[triangle->indices.v0_id - 1];
        parser::Vec3f t_b = scene.vertex_data[triangle->indices.v1_id - 1];
        parser::Vec3f t_c = scene.vertex_data[triangle->indices.v2_id - 1];


        double tmA = t_a.x - t_b.x;
        double tmB = t_a.y - t_b.y;
        double tmC = t_a.z - t_b.z;
        double tmD = t_a.x - t_c.x;
        double tmE = t_a.y - t_c.y;
        double tmF = t_a.z - t_c.z;
        double tmG = ray->d.x;
        double tmH = ray->d.y;
        double tmI = ray->d.z;
        double tmJ = t_a.x - ray->e.x;
        double tmK = t_a.y - ray->e.y;
        double tmL = t_a.z - ray->e.z;
        double ei_hf = tmE * tmI - tmH * tmF;
        double gf_di = tmG * tmF - tmD * tmI;
        double dh_eg = tmD * tmH - tmE * tmG;
        double ak_jb = tmA * tmK - tmJ * tmB;
        double jc_al = tmJ * tmC - tmA * tmL;
        double bl_kc = tmB * tmL - tmK * tmC;
        double determinantM = tmA * ei_hf + tmB * gf_di + tmC * dh_eg;
        double t = -(tmF * ak_jb + tmE * jc_al + tmD * bl_kc) / determinantM;


        if (t < t_0 || t > t_max) {
            return 0;
        }

        if (mode == 1 && (getTriangleNorm(triangle) * ray->d) > 0){
            return 0;
        }

        double gama = (tmI * ak_jb + tmH * jc_al + tmG * bl_kc) / determinantM;
        if (gama < 0 || gama > 1) {
            return 0;
        }
        double beta = (tmJ * ei_hf + tmK * gf_di + tmL * dh_eg) / determinantM;
        if (beta < 0 || (beta + gama > 1)) {
            return 0;
        }

        return t;
    }

    void objCatcher(Ray *ray, Obj *obj, unsigned char mode) {
        parser::Sphere *sphere;
        parser::Triangle *triangle;
        double t;
        for (unsigned long k = 0; k < sphereCount; ++k) { // for all spheres
            sphere = &scene.spheres[k];
            t = sphereIntersect(ray, sphere, 0, obj->t);
            if (t != 0) {
                obj->objPtr = sphere;
                obj->objType = SPHERE;
                obj->t = t;
            }
        }

        for (unsigned long k = 0; k < triangleCount; ++k) { // for all triangles
            triangle = &scene.triangles[k];
            t = triangleIntersect(ray, triangle, 0, obj->t, mode);
            if (t != 0) {
                obj->objPtr = triangle;
                obj->objType = TRIANGLE;
                obj->t = t;
            }
        }


    }


    parser::Vec3f getShading(Obj *obj, Ray *ray, int lap) {
        parser::PointLight light;
        parser::Material material = scene.materials[obj->getMaterialId() - 1];
        parser::Vec3f ambientShading = shadingProduct(material.ambient, scene.ambient_light);
        parser::Vec3f diffuseShading = {0, 0, 0};
        parser::Vec3f specularShading = {0, 0, 0};
        parser::Vec3f reflectance = {0, 0, 0};
        parser::Vec3f w_i, w_e, h;
        parser::Vec3f norm;
        parser::Vec3f intersectionPoint;
        Ray olRay;
        Obj interObj(nullptr, EMPTY, DBL_MAX);
        double lightDistance2 = 0;

        intersectionPoint = ray->compute_point_at(obj->t);
        norm = obj->getNorm(intersectionPoint);
        w_e = ray->e - intersectionPoint;
        w_e = w_e * ((float) 1.0 / getVecLength(w_e));


        for (int i = 0; i < pointLightsCount; ++i) {
            light = scene.point_lights[i];

            w_i = light.position - intersectionPoint;
            w_i = w_i * ((float) 1.0 / getVecLength(w_i));




//            if getirilerek araya kaçan objelerden dolayı oluşan fazlalık kaldırılacak.
            olRay.e = intersectionPoint + w_i * scene.shadow_ray_epsilon;
            olRay.d = w_i;

            if (olRay.d.x != 0) {
                interObj.t = (light.position - olRay.e).x / olRay.d.x;
            } else if (olRay.d.y != 0) {
                interObj.t = (light.position - olRay.e).y / olRay.d.y;
            } else if (olRay.d.z != 0) {
                interObj.t = (light.position - olRay.e).z / olRay.d.z;
            }


            interObj.objType = EMPTY;
            interObj.objPtr = nullptr;
            objCatcher(&olRay, &interObj, 0);
//            std::cout << (int)interObj.objType << "\n";
            if (interObj.objType != EMPTY) {
                continue;
            }

            lightDistance2 = getDistance2(intersectionPoint, light.position);

            diffuseShading = diffuseShading +
                             shadingProduct(std::max((float) 0.0, w_i * norm) * material.diffuse,
                                            light.intensity * (float) (1.0 / lightDistance2)
                             );


            h = w_e + w_i;
            h = h * ((float) 1.0 / getVecLength(h));

            specularShading = specularShading +
                              shadingProduct(
                                      pow(std::max((float) 0.0, norm * h), material.phong_exponent) * material.specular,
                                      light.intensity * (float) (1.0 / lightDistance2)
                              );


        }

        if ((material.mirror.x != 0 || material.mirror.y != 0 || material.mirror.z != 0) &&
            lap < scene.max_recursion_depth) {
            // that means we need to made reflection
            Ray rayRef;
            Obj objRef(nullptr, EMPTY, DBL_MAX);
            rayRef.d = -w_e + 2 * norm * (norm * w_e);
            rayRef.d = rayRef.d * ((float) 1.0 / getVecLength(rayRef.d));
            rayRef.e = intersectionPoint + rayRef.d * scene.shadow_ray_epsilon;
            //                w r = -w o + 2ncosΘ = -w o + 2n(n.w o )
            objCatcher(&rayRef, &objRef, 1);
            if (objRef.objType != EMPTY) {
                //                    biz eğer direkt olarak ışık kaynağını kesersek nolacak. cornellbox örneğindeki top view deki farklılığın sebebi bu olabilir.
                reflectance = shadingProduct(material.mirror,
                                             this->getShading(&objRef, &rayRef, lap + 1));
            }

        }

        return ambientShading + diffuseShading + specularShading + reflectance;
    }

    parser::Vec3i normalizeRGB(parser::Vec3f v) {
        parser::Vec3i ret;
        ret.x = (int) round(v.x);
        ret.y = (int) round(v.y);
        ret.z = (int) round(v.z);

        if (v.x > 255) {
            ret.x = 255;
        }
        if (v.y > 255) {
            ret.y = 255;
        }
        if (v.z > 255) {
            ret.z = 255;
        }
        return ret;
    }
};


unsigned long prepareMeshFaces() {
    parser::Mesh *mesh = nullptr;
    parser::Face face;
    unsigned long meshCount = scene.meshes.size(), faceCount;

    for (unsigned long k = 0; k < meshCount; ++k) {
        mesh = &scene.meshes[k];
        faceCount = mesh->faces.size();

        for (unsigned long l = 0; l < faceCount; ++l) {
            face = mesh->faces[l];
            parser::Triangle newTriangle;
            newTriangle.material_id = mesh->material_id;
            newTriangle.indices.v0_id = face.v0_id;
            newTriangle.indices.v1_id = face.v1_id;
            newTriangle.indices.v2_id = face.v2_id;
            scene.triangles.push_back(newTriangle);
        }

    }
    return scene.triangles.size();
}

void prepareCameras() {
    parser::Camera *camera = nullptr;
    parser::Vec3f u;
    for (unsigned long k = 0; k < cameraCount; ++k) {
        camera = &(scene.cameras[k]);
        camera->up = (camera->up * (1 / getVecLength(camera->up)));
        camera->gaze = (camera->gaze * (1 / getVecLength(camera->gaze)));
        u = camera->gaze % camera->up;
        u = (u * (1 / getVecLength(u)));
        camera->up = u % camera->gaze;
        camera->up = (camera->up * (1 / getVecLength(camera->up)));
    }
}

int main(int argc, char *argv[]) {
    // Sample usage for reading an XML scene file
    scene.loadFromXml(argv[1]);

    parser::Camera camera;

    cameraCount = scene.cameras.size();
    sphereCount = scene.spheres.size();
    triangleCount = prepareMeshFaces();
    pointLightsCount = scene.point_lights.size();
    prepareCameras();
    for (unsigned long cameraIndex = 0; cameraIndex < cameraCount; ++cameraIndex) {

        camera = scene.cameras[cameraIndex];
        Painter painter(&camera);
        painter.execute();

    }

    return 0;
}
