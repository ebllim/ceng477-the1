
#ifndef THE1_RAYTRACER_H
#define THE1_RAYTRACER_H

#include "parser.h"

#define EMPTY 0
#define SPHERE 1
#define TRIANGLE 2
#define MESH 3
parser::Scene scene;


float getVecLength(parser::Vec3i v) {
    return (float) sqrt((v.x) * (v.x) + (v.y) * (v.y) + (v.z) * (v.z));
};

float getVecLength(parser::Vec3f v) {
    return (float) sqrt((v.x) * (v.x) + (v.y) * (v.y) + (v.z) * (v.z));
};

template<typename T>
double getDistance2(T v, T u) {

    return pow(v.x - u.x, 2) + pow(v.y - u.y, 2) + pow(v.z - u.z, 2);
}

parser::Vec3f shadingProduct(parser::Vec3f c, parser::Vec3f v) {
    return (parser::Vec3f) {c.x * v.x, c.y * v.y, c.z * v.z};
}

parser::Vec3f shadingProduct(parser::Vec3f c, parser::Vec3i v) {
    return (parser::Vec3f) {c.x * v.x, c.y * v.y, c.z * v.z};
}

parser::Vec3i shadingProduct(parser::Vec3i c, parser::Vec3i v) {
    return (parser::Vec3i) {c.x * v.x, c.y * v.y, c.z * v.z};
}

parser::Vec3i Vec3fTo3i(parser::Vec3f v) {
    return (parser::Vec3i) {(int) round(v.x), (int) round(v.y), (int) round(v.z)};
};

template<typename T>
parser::Vec3f operator*(T f, parser::Vec3f v) {
    return (parser::Vec3f) {(float) f * v.x, (float) f * v.y, (float) f * v.z};
};

template<typename T>
parser::Vec3f operator*(parser::Vec3f v, T f) {
    return (parser::Vec3f) {f * v.x, f * v.y, f * v.z};
};

template<typename T>
parser::Vec4f operator*(T f, parser::Vec4f v) {
    return (parser::Vec4f) {f * v.x, f * v.y, f * v.z, f * v.w};
};

template<typename T>
parser::Vec4f operator*(parser::Vec4f v, T f) {
    return (parser::Vec4f) {f * v.x, f * v.y, f * v.z, f * v.w};
};


template<typename T>
parser::Vec3i operator*(T i, parser::Vec3i v) {
    return (parser::Vec3i) {i * v.x, i * v.y, i * v.z};
};

template<typename T>
parser::Vec3i operator*(parser::Vec3i v, T i) {
    return (parser::Vec3i) {i * v.x, i * v.y, i * v.z};
};

parser::Vec3i operator*(parser::Vec3i v, double i) {
    return (parser::Vec3i) {(int) round(i) * v.x, (int) round(i) * v.y, (int) round(i) * v.z};
};

parser::Vec3i operator*(parser::Vec3i v, float i) {
    return (parser::Vec3i) {(int) round(i) * v.x, (int) round(i) * v.y, (int) round(i) * v.z};
};

int operator*(parser::Vec3i v1, parser::Vec3i v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
};

float operator*(parser::Vec3f v1, parser::Vec3f v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
};

float operator*(parser::Vec4f v1, parser::Vec4f v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w;
};

parser::Vec3i operator+(parser::Vec3i v1, parser::Vec3i v2) {
    return (parser::Vec3i) {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
};

parser::Vec3f operator+(parser::Vec3f v1, parser::Vec3f v2) {
    return (parser::Vec3f) {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
};

parser::Vec4f operator+(parser::Vec4f v1, parser::Vec4f v2) {
    return (parser::Vec4f) {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w};
};

parser::Vec3f operator+(parser::Vec3f v1, parser::Vec3i v2) {
    return (parser::Vec3f) {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
};

parser::Vec3f operator+(parser::Vec3i v1, parser::Vec3f v2) {
    return (parser::Vec3f) {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
};

parser::Vec3i operator-(parser::Vec3i v1, parser::Vec3i v2) {
    return (parser::Vec3i) {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
};

parser::Vec3f operator-(parser::Vec3f v1, parser::Vec3f v2) {
    return (parser::Vec3f) {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
};

parser::Vec4f operator-(parser::Vec4f v1, parser::Vec4f v2) {
    return (parser::Vec4f) {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w};
};

parser::Vec3f operator-(parser::Vec3f v1, parser::Vec3i v2) {
    return (parser::Vec3f) {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
};

parser::Vec3f operator-(parser::Vec3i v1, parser::Vec3f v2) {
    return (parser::Vec3f) {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
};

parser::Vec3i operator-(parser::Vec3i v) {
    return (parser::Vec3i) {-v.x, -v.y, -v.z};
};

parser::Vec3f operator-(parser::Vec3f v) {
    return (parser::Vec3f) {-v.x, -v.y, -v.z};
};

parser::Vec4f operator-(parser::Vec4f v) {
    return (parser::Vec4f) {-v.x, -v.y, -v.z, -v.w};
};

parser::Vec3i operator%(parser::Vec3i v1, parser::Vec3i v2) {
    return (parser::Vec3i) {
            v1.y * v2.z - v1.z * v2.y,
            v1.z * v2.x - v1.x * v2.z,
            v1.x * v2.y - v1.y * v2.x};
};

parser::Vec3f operator%(parser::Vec3f v1, parser::Vec3f v2) {
    return (parser::Vec3f) {
            v1.y * v2.z - v1.z * v2.y,
            v1.z * v2.x - v1.x * v2.z,
            v1.x * v2.y - v1.y * v2.x};
};


template<typename T>
double pow2Dot(T v1) {
    return v1 * v1;
}

class Ray {
public:
    parser::Vec3f e;
    parser::Vec3f d;

    parser::Vec3f compute_point_at(double t) {
        return (this->e + (t * this->d));
    }
};


class Obj {
public:
    Obj(void *pVoid, int i, double d) {
        this->objPtr = pVoid;
        this->objType = EMPTY;
        this->t = d;
    }

    void *objPtr;
    unsigned char objType;
    double t;

    int getMaterialId() {
        switch (this->objType) {
            case SPHERE:
                return ((parser::Sphere *) (this->objPtr))->material_id;
            case TRIANGLE:
                return ((parser::Triangle *) (this->objPtr))->material_id;
            default:
                return -1;
        }
    }

    parser::Vec3f getNorm(parser::Vec3f intersectionPoint) {
        parser::Vec3f ret = {0, 0, 0};
        switch (this->objType) {
            case SPHERE:
                ret = (intersectionPoint - scene.vertex_data[((parser::Sphere *) this->objPtr)->center_vertex_id - 1])
                      * ((float) 1.0 / ((parser::Sphere *) this->objPtr)->radius);
                return ret;
            case TRIANGLE:
                ret = (scene.vertex_data[((parser::Triangle *) this->objPtr)->indices.v1_id - 1] -
                       scene.vertex_data[((parser::Triangle *) this->objPtr)->indices.v0_id - 1])
                      %
                      (scene.vertex_data[((parser::Triangle *) this->objPtr)->indices.v2_id - 1] -
                       scene.vertex_data[((parser::Triangle *) this->objPtr)->indices.v0_id - 1]);
                ret = ret * ((float) 1.0 / getVecLength(ret));
                return ret;
            default:
                return ret;
        }
    }

};

parser::Vec3f getTriangleNorm(parser::Triangle *triangle) {
    parser::Vec3f ret = (scene.vertex_data[triangle->indices.v1_id - 1] -
           scene.vertex_data[triangle->indices.v0_id - 1])
          %
          (scene.vertex_data[triangle->indices.v2_id - 1] -
           scene.vertex_data[triangle->indices.v0_id - 1]);
    ret = ret * ((float) 1.0 / getVecLength(ret));
    return ret;
}

#endif //THE1_RAYTRACER_H
